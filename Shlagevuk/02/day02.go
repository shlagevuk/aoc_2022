package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

// Readln returns a single line (without the ending \n)
// from the input buffered reader.
// An error is returned iff there is an error with the
// buffered reader.
func Readln(r *bufio.Reader) (string, error) {
	var (
		isPrefix bool  = true
		err      error = nil
		line, ln []byte
	)
	for isPrefix && err == nil {
		line, isPrefix, err = r.ReadLine()
		ln = append(ln, line...)
	}
	return string(ln), err
}

// Print array
func printSlice(s []int) {
	fmt.Printf("len=%d cap=%d %v\n", len(s), cap(s), s)
}
func main() {
	fmt.Println("result part one: " + strconv.Itoa(part_one()))
	fmt.Println("result part two: " + strconv.Itoa(part_two()))

}
func part_one() int {
	// open input file
	f, err := os.Open("input.txt")
	if err != nil {
		fmt.Printf("error opening file: %v\n", err)
		os.Exit(1)
	}
	//create reader
	r := bufio.NewReader(f)

	// A rock
	// B paper
	// C cissor
	// X rock 1
	// Y paper 2
	// Z cissor 3
	// win == 6
	// draw == 3
	// loose == 0
	//main loop
	s, e := Readln(r)
	score := 0
	for e == nil {
		play := strings.Split(s, " ")
		switch play[1] {
		case "X":
			score += 1
			if play[0] == "A" {
				score += 3
			} else if play[0] == "C" {
				score += 6
			}
		case "Y":
			score += 2
			if play[0] == "B" {
				score += 3
			} else if play[0] == "A" {
				score += 6
			}
		case "Z":
			score += 3
			if play[0] == "C" {
				score += 3
			} else if play[0] == "B" {
				score += 6
			}
		}
		s, e = Readln(r)
	}

	f.Close()
	return score
}

func part_two() int {
	// open input file
	f, err := os.Open("input.txt")
	if err != nil {
		fmt.Printf("error opening file: %v\n", err)
		os.Exit(1)
	}
	//create reader
	r := bufio.NewReader(f)

	// A rock
	// B paper
	// C cissor
	// X loose
	// Y draw
	// Z win
	// win == 6
	// draw == 3
	// loose == 0
	//main loop
	s, e := Readln(r)
	score := 0
	for e == nil {
		play := strings.Split(s, " ")
		switch play[0] {
		case "A":
			if play[1] == "X" {
				score += 0 + 3
			} else if play[1] == "Y" {
				score += 3 + 1
			} else if play[1] == "Z" {
				score += 6 + 2
			}
		case "B":
			if play[1] == "X" {
				score += 0 + 1
			} else if play[1] == "Y" {
				score += 3 + 2
			} else if play[1] == "Z" {
				score += 6 + 3
			}
		case "C":
			if play[1] == "X" {
				score += 0 + 2
			} else if play[1] == "Y" {
				score += 3 + 3
			} else if play[1] == "Z" {
				score += 6 + 1
			}
		}
		s, e = Readln(r)
	}

	f.Close()
	return score
}
