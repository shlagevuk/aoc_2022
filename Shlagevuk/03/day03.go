package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

// Readln returns a single line (without the ending \n)
// from the input buffered reader.
// An error is returned iff there is an error with the
// buffered reader.
func Readln(r *bufio.Reader) (string, error) {
	var (
		isPrefix bool  = true
		err      error = nil
		line, ln []byte
	)
	for isPrefix && err == nil {
		line, isPrefix, err = r.ReadLine()
		ln = append(ln, line...)
	}
	return string(ln), err
}

// Print array
func printSlice(s []int) {
	fmt.Printf("len=%d cap=%d %v\n", len(s), cap(s), s)
}
func main() {
	fmt.Println("result part one: " + strconv.Itoa(part_one()))
	fmt.Println("result part two: " + strconv.Itoa(part_two()))

}
func part_one() int {
	// open input file
	f, err := os.Open("input.txt")
	if err != nil {
		fmt.Printf("error opening file: %v\n", err)
		os.Exit(1)
	}
	//create reader
	r := bufio.NewReader(f)

	//main loop
	s, e := Readln(r)
	score := 0
	lenght := 0
	var first string
	var second string
	var letterpos int
	for e == nil {
		lenght = len(s)
		first = s[0 : lenght/2]
		second = s[(lenght / 2):lenght]
		letterpos = strings.IndexAny(first, second)
		//fmt.Println(letterpos)
		if letterpos >= 0 {
			//fmt.Println("--------")
			//fmt.Println(s[letterpos])
			if s[letterpos]-'a' < 26 {
				score += int(s[letterpos] - 'a' + 1)
				//fmt.Println(s[letterpos] - 'a' + 1)
			} else {
				score += int(s[letterpos] - 'A' + 27)
				//fmt.Println(s[letterpos] - 'A' + 27)
			}
			//fmt.Println((string(s[letterpos])))
		}
		s, e = Readln(r)
	}

	f.Close()
	return score
}

func part_two() int {
	// open input file
	f, err := os.Open("input.txt")
	if err != nil {
		fmt.Printf("error opening file: %v\n", err)
		os.Exit(1)
	}
	////create reader
	r := bufio.NewReader(f)

	////main loop
	//main loop
	s, e := Readln(r)
	score := 0
	var first string
	var first_index int
	var second string
	var third string
	for e == nil {
		first = s
		second, e = Readln(r)
		third, e = Readln(r)
	comparison:
		for i := 0; i < len(first); i++ {
			for j := 0; j < len(second); j++ {
				if first[i] == second[j] {
					for k := 0; k < len(third); k++ {
						if first[i] == third[k] {
							//fmt.Println("found! " + string(first[i]))
							first_index = i
							break comparison
						}
					}
				}
			}
		}

		if first[first_index]-'a' < 26 {
			score += int(first[first_index] - 'a' + 1)
			//fmt.Println(first[first_index] - 'a' + 1)
		} else {
			score += int(first[first_index] - 'A' + 27)
			//fmt.Println(first[first_index] - 'A' + 27)
		}
		//fmt.Println(first[first_index])
		//fmt.Println((string(first[first_index])))
		s, e = Readln(r)
	}

	f.Close()
	return score

}
