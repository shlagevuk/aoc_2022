package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
	"strconv"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

// Readln returns a single line (without the ending \n)
// from the input buffered reader.
// An error is returned iff there is an error with the
// buffered reader.
func Readln(r *bufio.Reader) (string, error) {
	var (
		isPrefix bool  = true
		err      error = nil
		line, ln []byte
	)
	for isPrefix && err == nil {
		line, isPrefix, err = r.ReadLine()
		ln = append(ln, line...)
	}
	return string(ln), err
}
func printSlice(s []int) {
	fmt.Printf("len=%d cap=%d %v\n", len(s), cap(s), s)
}
func main() {
	fmt.Println("result part one: " + strconv.Itoa(part_one()))
	fmt.Println("result part two: " + strconv.Itoa(part_two()))

}
func part_one() int {
	// open input file
	f, err := os.Open("input.txt")
	if err != nil {
		fmt.Printf("error opening file: %v\n", err)
		os.Exit(1)
	}
	//create reader
	r := bufio.NewReader(f)

	//main loop
	var values []int
	s, e := Readln(r)
	value := 0
	v := 0
	for e == nil {
		v, e = strconv.Atoi(s)
		if e != nil {
			values = append(values, value)
			value = 0
		} else {
			value += v
		}
		s, e = Readln(r)
	}
	// get max
	max := 0
	for i, e := range values {
		if i == 0 || e > max {
			max = e
		}
	}

	f.Close()
	return max
}

func part_two() int {
	// open input file
	f, err := os.Open("input.txt")
	if err != nil {
		fmt.Printf("error opening file: %v\n", err)
		os.Exit(1)
	}
	//create reader
	r := bufio.NewReader(f)

	//main loop
	var values []int
	s, e := Readln(r)
	value := 0
	v := 0
	for e == nil {
		v, e = strconv.Atoi(s)
		if e != nil {
			values = append(values, value)
			value = 0
		} else {
			value += v
		}
		s, e = Readln(r)
	}
	sort.Sort(sort.Reverse(sort.IntSlice(values)))
	return values[0] + values[1] + values[2]
}
